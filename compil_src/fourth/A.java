package fourth;

public class A {
    String label = "42";
    
    public A () {}

    public A (String label) {
        this.label = label;
    }

    public String toString () {
        return this.label;
    }
}
