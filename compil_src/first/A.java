public class A {
    String label = "42";
    
    A () {}

    A (String label) {
        this.label = label;
    }

    public String toString () {
        return this.label;
    }
    
    public static void main(String[] args) {
        A a1 = new A(), a2 = new A("69");
        System.out.println(a1 + " " + a2);
    }
}
