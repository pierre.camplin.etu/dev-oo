public class A {
    String label = "42";
    
    A () {}

    A (String label) {
        this.label = label;
    }

    public String toString () {
        return this.label;
    }
}
