import controlpkg1.A;
import controlpkg2.B;
import controlpkg2.sspkg.C;
import controlpkg1.D;

public class Main {
    public static void main (String[] args) {
        A a = new A("69");
        B b = new B();
        C c = new C("420", 420);
        D d = new D();

        System.out.println(a + "\n" + b + "\n" + c + "\n" + d.getSomeText());
    }
}
