package td.td07;

import java.util.HashMap;

public class PhoneBook {
    private HashMap<String, PhoneNumber> directory;

    public PhoneBook() {
        this.directory = new HashMap<>();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PhoneBook{");
        sb.append("directory=").append(directory);
        sb.append('}');
        return sb.toString();
    }
}
