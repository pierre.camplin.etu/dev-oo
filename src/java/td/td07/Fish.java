package td.td07;

public class Fish implements IAnimal{
    private static final String SHOUT = "Blob!";
    private static final int QTY = 1;
    private static final boolean PET = false;

    @Override
    public String shout() {
        return SHOUT;
    }

    @Override
    public int foodAmount() {
        return QTY;
    }

    @Override
    public boolean isPet() {
        return PET;
    }
}
