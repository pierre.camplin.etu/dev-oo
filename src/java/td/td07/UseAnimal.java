package td.td07;

import java.util.ArrayList;

public class UseAnimal {
    private static ArrayList<IAnimal> zoo = new ArrayList<>();

    public static void main(String[] args) {
        zoo.add(new Pig(20));
        zoo.add(new Fish());
        zoo.add(new Tiger());

        for (IAnimal animal:zoo) {
            System.out.println(animal.shout() + animal.foodAmount() + animal.isPet());
        }
    }
}
