package td.td07;

import java.util.Random;

public class Tiger implements IAnimal{
    private static final String SHOUT = "Purr";
    private static final int QTYMAX = 20;
    private static final Random RAND = new Random();
    private static final boolean PET = false;

    @Override
    public String shout() {
        return SHOUT;
    }

    @Override
    public int foodAmount() {
        if (RAND.nextInt(2) == 0) return QTYMAX;
        else return 0;
    }

    @Override
    public boolean isPet() {
        return PET;
    }
}
