package td.td07;

public interface IAnimal {
    public String shout ();
    public int foodAmount ();
    public boolean isPet();
}
