package td.td07;

public class PhoneNumber {
    private int countryCode;
    private int areaCode;
    private int secteurCode;
    private int one;
    private int two;
    private int three;

    public PhoneNumber(int countryCode, int areaCode, int secteurCode, int one, int two, int three) {
        this.countryCode = countryCode;
        this.areaCode = areaCode;
        this.secteurCode = secteurCode;
        this.one = one;
        this.two = two;
        this.three = three;
    }

    public String standartFormat () {
        return  "" + this.secteurCode + this.areaCode + this.one + this.two + this.three;
    }

    public String internationalFormat () {
        return "+" + this.countryCode + this.standartFormat();

    }

    @Override
    public String toString() {
        return "Standard Format : " + this.standartFormat() + "\nInternational Format : " + this.internationalFormat();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhoneNumber that)) return false;

        if (countryCode != that.countryCode) return false;
        if (areaCode != that.areaCode) return false;
        if (secteurCode != that.secteurCode) return false;
        if (one != that.one) return false;
        if (two != that.two) return false;
        return three == that.three;
    }

    @Override
    public int hashCode() {
        int result = countryCode;
        result = 31 * result + areaCode;
        result = 31 * result + secteurCode;
        result = 31 * result + one;
        result = 31 * result + two;
        result = 31 * result + three;
        return result;
    }
}
