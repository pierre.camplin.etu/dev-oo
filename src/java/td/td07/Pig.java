package td.td07;

public class Pig implements IAnimal{
    private final static String SHOUT = "Groink!";
    private final static boolean PET = true;
    private int weight;
    private int mealSize;

    public Pig (int weight) {
        this.weight = weight;
        this.mealSize = 20 + weight / 2;
    }

    @Override
    public String shout() {
        return SHOUT;
    }

    @Override
    public int foodAmount() {
        return mealSize;
    }

    @Override
    public boolean isPet() {
        return PET;
    }

    public void rollingInMud () {
        System.out.println("Pig is rolling in mud");
    }
}
