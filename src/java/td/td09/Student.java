package td.td09;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Student implements Iterable<Double>, Comparable<Student>{
    private Person person;
    private String INE;
    private static int cptStudent = 0;
    public static boolean civilianDisplay = true;
    private Map<Mat, Double> grades;

    public Student(String name, LocalDate birth, Map<Mat, Double> grades) {
        this.INE = String.valueOf(Student.cptStudent++);
        this.person = new Person(name, birth);
        this.grades = grades;
    }

    public Student (String name, LocalDate birth) {
        this(name,birth,new HashMap<>());
    }

    public void addGrade (Mat mat, double grade) {
        this.grades.put(mat, grade);
    }

    public void addGrade (String mat, double grade) {
        this.addGrade(Mat.valueOf(mat.toUpperCase()), grade);
    }

    public double computeOverallGrade () {
        double sum = 0;
        if (this.grades.isEmpty()) return -1;
        else {
            for (Double grade: this.grades.values()) {
                sum += grade;
            }
            return sum/this.grades.size();
        }
    }

    private String toStringCivilian () {
        return this.person.toString();
    }

    private String toStringPedagogic () {
        return this.INE + ":" + this.person.getName() + ":" + this.computeOverallGrade() + "-->" + this.grades;
    }

    @Override
    public String toString() {
        if (Student.civilianDisplay) return this.toStringCivilian();
        return this.toStringPedagogic();
    }

    @NotNull
    @Override
    public Iterator<Double> iterator() {
        return this.grades.values().iterator();
    }

    @Override
    public void forEach(Consumer<? super Double> action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator<Double> spliterator() {
        return Iterable.super.spliterator();
    }

    @Override
    public int compareTo(@NotNull Student other) {
        return Double.compare(this.computeOverallGrade(), other.computeOverallGrade());
    }
}
