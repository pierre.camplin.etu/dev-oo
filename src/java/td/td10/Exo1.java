package td.td10;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Exo1 {
    static String myPath = "";
    static String sourceFile = "truc";

    public static void main(String[] args) {
        try {
            FileReader fr = new FileReader("truc");
            try {
                int c = fr.read();
                while (c != -1) {
                    System.out.println((char) c);
                    c = fr.read();
                }
            } catch (IOException e1) {
                System.err.println("Reading error:" + e1.getMessage());
            }
        } catch (FileNotFoundException e2) {
            System.err.println("File Not Found");
        }
    }
}
