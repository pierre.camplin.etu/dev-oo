package td.td10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class KeyboardInput {
    public static void inputToDivide() {
        int operand1 = 0, operand2 = 1, result = 0;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Write a first value:");
            operand1 = Integer.valueOf(br.readLine());
            System.out.println("Write a second value:");
            operand2 = Integer.valueOf(br.readLine());
            result = operand1 / operand2;
        } catch (NumberFormatException e) {
            System.err.println("Casting failure");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("IO Failure");
            e.printStackTrace();
        } catch (ArithmeticException e) {
            result = operand1;
        }
        System.out.println(result);
    }

    public static void main(String[] args) {
        inputToDivide();
    }
}
