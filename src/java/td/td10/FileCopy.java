package td.td10;

import org.jetbrains.annotations.NotNull;
import java.io.*;

public class FileCopy {
    public static void main(String @NotNull [] args) {
        try {
            if (args.length != 2) throw new WrongUsageException();
            else {
                File f1 = new File(args[0]);
                File f2 = new File(args[1]);
                if (!f1.exists()) throw new FileNotFoundException();
                if (!f2.exists()) throw new ExistingFileException();
                FileInputStream fis1 = new FileInputStream(f1);
                FileOutputStream fos2 = new FileOutputStream(f2);
                while (fis1.available() > 0) {
                    fos2.write(fis1.read());
                }
                fos2.flush();
                fis1.close();
                fos2.close();
            }
        }
        catch (WrongUsageException | FileNotFoundException | ExistingFileException e) {e.printStackTrace();System.exit(0);}
        catch (IOException e) {e.printStackTrace();}
    }
}
