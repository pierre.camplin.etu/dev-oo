package td.td08;

/**
 * The type Packet.
 */
public class Packet extends Mail{
    private double weight;
    private static final double MAX_WEIGHT = 5000.0;

    /**
     * Instantiates a new Packet.
     *
     * @param express the express
     * @param address the address
     * @param weight  the weight
     */
    public Packet (boolean express, String address, double weight) {
        super(express, address);
        this.weight = weight;
    }

    @Override
    public boolean isValid () {
        return super.isValid() && this.weight < MAX_WEIGHT;
    }

    @Override
    public double frank () {
        if (!this.isValid()) return 0.0;
        double res = Mail.BASIS;
        res += this.weight % 100;
        if (this.express) res *= 2;
        return res;
    }

    @Override
    public String toString () {
        String res = super.toString().substring(4) + "Packet";
        res += "\tWeight : " + this.weight + "g";
        return res;
    }
}
