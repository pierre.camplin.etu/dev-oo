package td.td08;

/**
 * The type Registered letter.
 */
public class RegisteredLetter extends Mail{
    private String format;
    private static final double A4 = 2.0, OTHER = 3.0;

    /**
     * Instantiates a new Registered letter.
     *
     * @param express the express
     * @param address the address
     * @param format  the format
     */
    public RegisteredLetter (boolean express, String address, String format) {
        super(express, address);
        this.format = format;
    }

    @Override
    public String toString() {
        String res = super.toString();
        res += "\tFormat : " + this.format;
        return res;
    }

    @Override
    public double frank() {
        if (!this.isValid()) return 0.0;
        double res = Mail.BASIS;
        if (this.format.equals("A4")) res += A4;
        else res += OTHER;
        if (this.express) res *= 2;
        return res;
    }
}
