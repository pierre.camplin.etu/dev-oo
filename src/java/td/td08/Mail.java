package td.td08;

/**
 * The type Mail.
 */
public class Mail {
    /**
     * The constant BASIS.
     */
    public static final double BASIS = 1.5;
    /**
     * The Express.
     */
    protected boolean express;
    /**
     * The Address.
     */
    protected String address;

    /**
     * Instantiates a new Mail.
     *
     * @param express the express
     * @param address the address
     */
    public Mail(boolean express, String address) {
        this.express = express;
        this.address = address;
    }

    /**
     * Is valid boolean.
     *
     * @return the boolean
     */
    public boolean isValid () {
        return this.address != null && this.address.length() > 0;
    }

    /**
     * Frank double.
     *
     * @return the double
     */
    public double frank () {
        if (!this.isValid()) return 0;
        double res = BASIS;
        if (this.express) res *= 2.0;
        return res;
    }

    @Override
    public String toString() {
        String s = "Mail";
        if (!this.isValid()) s += " (invalide)";
        else if (this.express) s += " (express)";
        s += "\n\tDestination : " + this.address + "\n";
        s += "\tPrice : " + this.frank() + "\n";
        return s;
    }
}
