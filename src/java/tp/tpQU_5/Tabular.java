package tp.tpQU_5;

import java.util.Arrays;

public class Tabular {
    private Number[] tab;

    public Tabular(int size) {
        this.tab = new Number[size];
    }

    public void set (int idx, Number number) {
        this.tab[idx] = number;
    }

    public Number max () {
        Number max = Integer.MIN_VALUE;
        for (int i = 0; i < this.tab.length; i++) {
            if (this.tab[i].doubleValue() > max.doubleValue()) {
                max = this.tab[i].doubleValue();
            }
        }
        return max;
    }

    @Override
    public String toString() {
        return "Tabular{" +
                "tab=" + Arrays.toString(tab) +
                '}';
    }
}
