package tp.tpQU_5;

public class NumberFactory {
    public static Number number (int num) {
        return num;
    }
    public static Number number (long num) {
        return num;
    }
    public static Number number (float num) {
        return num;
    }
    public static Number number (double num) {
        return num;
    }
    public static Number number (int num,int denom) {
        return new Fraction(num, denom);
    }
}
