package tp.tp05;

import java.time.LocalDate;
import java.util.Objects;

/**
 * The type Book.
 */
public class Book {
    private static final int DURATION_MAX = 15;
    /**
     * The Code.
     */
    protected String code;
    /**
     * The Title.
     */
    protected String title;
    /**
     * The Author.
     */
    protected String author;
    /**
     * The Publication year.
     */
    protected int publicationYear;
    /**
     * The Borrower.
     */
    protected int borrower;
    /**
     * The Borrowing date.
     */
    protected LocalDate borrowingDate;

    /**
     * Instantiates a new Book.
     *
     * @param code            the code
     * @param title           the title
     * @param author          the author
     * @param publicationYear the publication year
     */
    public Book(String code, String title, String author, int publicationYear) {
        this.code = code;
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.borrower = 0;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Gets publication year.
     *
     * @return the publication year
     */
    public int getPublicationYear() {
        return publicationYear;
    }

    /**
     * Sets publication year.
     *
     * @param publicationYear the publication year
     */
    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    @Override
    public String toString() {
        return "Book [" + code + ":" + title + "->" + author + "," + publicationYear + ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;

        Book book = (Book) o;

        if (publicationYear != book.publicationYear) return false;
        if (!Objects.equals(code, book.code)) return false;
        if (!Objects.equals(title, book.title)) return false;
        return Objects.equals(author, book.author);
    }

    /**
     * Is available boolean.
     *
     * @return the boolean
     */
    public boolean isAvailable () {
        return this.borrower == 0;
    }

    /**
     * Borrow boolean.
     *
     * @param borrower the borrower
     * @return the boolean
     */
    public boolean borrow (int borrower) {
        if (this.isAvailable()) {
            this.borrower = borrower;
            this.borrowingDate = LocalDate.now();
            return true;
        }
        return false;
    }

    /**
     * Give back boolean.
     *
     * @return the boolean
     */
    public boolean giveBack () {
        if (!this.isAvailable()) {
            this.borrower = 0;
            this.borrowingDate = null;
            return true;
        }
        return false;
    }

    /**
     * Gets borrower.
     *
     * @return the borrower
     */
    public int getBorrower() {
        return borrower;
    }

    /**
     * Gets duration max.
     *
     * @return the duration max
     */
    public int getDurationMax() {
        return DURATION_MAX;
    }

    /**
     * Gets give back date.
     *
     * @return the give back date
     */
    public LocalDate getGiveBackDate () {
        return this.borrowingDate.plusDays(this.getDurationMax());
    }

    /**
     * Borrow again boolean.
     *
     * @param borrower the borrower
     * @return the boolean
     */
    public boolean borrowAgain (int borrower) {
        return this.borrow(borrower);
    }
}
