package tp.tp05;

import java.time.LocalDate;

/**
 * The type Perishable article.
 */
public class PerishableArticle extends Article{
    private LocalDate deadLine;
    private static final int DEFAULT_DEADLINE = 10;

    /**
     * Instantiates a new Perishable article.
     *
     * @param idRef         the id ref
     * @param label         the label
     * @param purchasePrice the purchase price
     * @param salePrice     the sale price
     * @param deadLine      the dead line
     */
    public PerishableArticle(String idRef, String label, double purchasePrice, double salePrice, LocalDate deadLine) {
        super(idRef, label, purchasePrice, salePrice);
        this.deadLine = deadLine;
    }

    /**
     * Instantiates a new Perishable article.
     *
     * @param idRef         the id ref
     * @param label         the label
     * @param purchasePrice the purchase price
     * @param deadLine      the dead line
     */
    public PerishableArticle(String idRef, String label, double purchasePrice, LocalDate deadLine) {
        super(idRef, label, purchasePrice);
        this.deadLine = deadLine;
    }

    /**
     * Instantiates a new Perishable article.
     *
     * @param idRef         the id ref
     * @param label         the label
     * @param purchasePrice the purchase price
     * @param salePrice     the sale price
     */
    public PerishableArticle(String idRef, String label, double purchasePrice, double salePrice) {
        super(idRef, label, purchasePrice, salePrice);
        this.deadLine = LocalDate.now().plusDays(DEFAULT_DEADLINE);
    }

    /**
     * Instantiates a new Perishable article.
     *
     * @param idRef         the id ref
     * @param label         the label
     * @param purchasePrice the purchase price
     */
    public PerishableArticle(String idRef, String label, double purchasePrice) {
        super(idRef, label, purchasePrice);
        this.deadLine = LocalDate.now().plusDays(DEFAULT_DEADLINE);
    }

    /**
     * Gets dead line.
     *
     * @return the dead line
     */
    public LocalDate getDeadLine() {
        return deadLine;
    }

    @Override
    public String toString () {
        String res = super.toString();
        res = "PerishableArticle" + res.substring(7, res.length() - 1) + "-->" + this.deadLine + ']';
        return res;
    }
}
