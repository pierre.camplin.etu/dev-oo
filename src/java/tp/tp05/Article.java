package tp.tp05;

/**
 * The type Article.
 */
public class Article {
    private static final double DEFAULT_MARGIN = 1.20;
    private String idRef;
    private String label;
    private final double PURCHASE_PRICE;
    private double salePrice;

    /**
     * Instantiates a new Article.
     *
     * @param idRef         the id ref
     * @param label         the label
     * @param purchasePrice the purchase price
     * @param salePrice     the sale price
     */
    public Article(String idRef, String label, double purchasePrice, double salePrice) {
        this.idRef = idRef;
        this.label = label;
        this.PURCHASE_PRICE = purchasePrice;
        this.salePrice = salePrice;
    }

    /**
     * Instantiates a new Article.
     *
     * @param idRef         the id ref
     * @param label         the label
     * @param purchasePrice the purchase price
     */
    public Article(String idRef, String label, double purchasePrice) {
        this.idRef = idRef;
        this.label = label;
        this.PURCHASE_PRICE = purchasePrice;
        this.salePrice = purchasePrice * DEFAULT_MARGIN;
    }

    /**
     * Gets sale price.
     *
     * @return the sale price
     */
    public double getSalePrice() {
        return salePrice;
    }

    /**
     * Sets sale price.
     *
     * @param salePrice the sale price
     */
    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    /**
     * Gets purchase price.
     *
     * @return the purchase price
     */
    public double getPurchasePrice() {
        return PURCHASE_PRICE;
    }

    /**
     * Gets margin.
     *
     * @return the margin
     */
    public double getMargin () {
        return this.salePrice - this.PURCHASE_PRICE;
    }

    @Override
    public String toString () {
        return "Article [" + this.idRef + ':' + this.label + '=' + this.PURCHASE_PRICE + "€/" + this.salePrice + "€]";
    }

    /**
     * Is perishable boolean.
     *
     * @return the boolean
     */
    public boolean isPerishable () {
        return this instanceof PerishableArticle;
    }
}
