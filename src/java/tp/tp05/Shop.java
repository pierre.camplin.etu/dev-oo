package tp.tp05;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * The type Shop.
 */
public class Shop {
    private ArrayList<Article> catalog;

    /**
     * Instantiates a new Shop.
     */
    public Shop () {
        this.catalog = new ArrayList<>();
    }

    /**
     * Add article boolean.
     *
     * @param article the article
     * @return the boolean
     */
    public boolean addArticle (Article article) {
        return this.catalog.add(article);
    }

    /**
     * Add article boolean.
     *
     * @param ats the ats
     * @return the boolean
     */
    public boolean addArticle (ArrayList<Article> ats) {
        return this.catalog.addAll(ats);
    }

    /**
     * Gets perishables.
     *
     * @return the perishables
     */
    public ArrayList<PerishableArticle> getPerishables () {
        ArrayList<PerishableArticle> res = new ArrayList<>();
        for (Article article : this.catalog) {
            if (article.isPerishable()) res.add((PerishableArticle) article);
        }
        return res;
    }

    /**
     * Gets nb article.
     *
     * @return the nb article
     */
    public int getNbArticle () {
        int nbArticle = 0;
        for (Article article : this.catalog) {
            if (article != null) nbArticle++;
        }
        return nbArticle;
    }

    /**
     * Gets nb perishable article.
     *
     * @return the nb perishable article
     */
    public int getNbPerishableArticle () {
        ArrayList<PerishableArticle> tmp = this.getPerishables();
        int nbPerishableArticle = 0;
        for (PerishableArticle perishableArticle : tmp) {
            if (perishableArticle != null) nbPerishableArticle++;
        }
        return nbPerishableArticle;
    }

    /**
     * Discount perishable.
     *
     * @param threshold the threshold
     * @param rate      the rate
     */
    public void discountPerishable (LocalDate threshold, double rate) {
        for (Article article : this.catalog) {
            if (article.isPerishable()) {
                PerishableArticle tmp = (PerishableArticle) article;
                if (tmp.getDeadLine().isBefore(threshold)) article.setSalePrice(article.getSalePrice() * rate);
            }
        }
    }

    /**
     * Discount not perishable.
     *
     * @param rate the rate
     */
    void discountNotPerishable (double rate) {
        for (Article article : this.catalog) {
            if (!article.isPerishable()) {
                article.setSalePrice(article.getSalePrice() * rate);
            }
        }
    }

    /**
     * Most profitable article.
     *
     * @return the article
     */
    public Article mostProfitable () {
        double maxMargin = 0.0;
        int max_idx = 0;
        for (int i = 0; i < this.catalog.size(); i++) {
            if (this.catalog.get(i).getMargin() > maxMargin) {
                maxMargin = this.catalog.get(i).getMargin();
                max_idx = i;
            }
        }
        return this.catalog.get(max_idx);
    }
}
