package tp.tp05;

import java.util.ArrayList;

/**
 * The type Library.
 */
public class Library {
    private ArrayList<Book> catalog;

    /**
     * Instantiates a new Library.
     */
    public Library () {
        this.catalog = new ArrayList<>();
    }

    /**
     * Gets book.
     *
     * @param code the code
     * @return the book
     */
    public Book getBook(String code) {
        boolean trouve = false;
        int i = 0;
        while (!trouve && i < this.catalog.size()) {
            if (this.catalog.get(i).getCode().equals(code)) trouve = true;
            i++;
        }
        if (!trouve && i == this.catalog.size()) return null;
        return this.catalog.get(i - 1);
    }

    /**
     * Add book boolean.
     *
     * @param book the book
     * @return the boolean
     */
    public boolean addBook (Book book) {
        return this.catalog.add(book);
    }

    /**
     * Remove book boolean.
     *
     * @param code the code
     * @return the boolean
     */
    public boolean removeBook (String code) {
        return this.catalog.remove(this.getBook(code));
    }

    /**
     * Remove book boolean.
     *
     * @param book the book
     * @return the boolean
     */
    public boolean removeBook (Book book) {
        return this.catalog.remove(book);
    }

    @Override
    public String toString() {
        return catalog.toString();
    }

    /**
     * Borrowings string.
     *
     * @return the string
     */
    public String borrowings () {
        String res = "";
        for (Book book : this.catalog) {
            if (!book.isAvailable()) res += '(' + book.getCode() + ")--" + book.getBorrower();
        }
        return res;
    }

    /**
     * Borrow boolean.
     *
     * @param code     the code
     * @param borrower the borrower
     * @return the boolean
     */
    public boolean borrow (String code, int borrower) {
        return this.getBook(code).borrow(borrower);
    }

    /**
     * Give back boolean.
     *
     * @param code the code
     * @return the boolean
     */
    public boolean giveBack (String code) {
        return this.getBook(code).giveBack();
    }

    /**
     * Stock size int.
     *
     * @return the int
     */
    public int stockSize () {
        int size = 0;
        for (Book book : this.catalog) {
            if (book != null) size++;
        }
        return size;
    }

    /**
     * Borrowed book number int.
     *
     * @return the int
     */
    public int borrowedBookNumber () {
        int size = 0;
        for (Book book : this.catalog) {
            if (book != null && !book.isAvailable()) size++;
        }
        return size;
    }
}
