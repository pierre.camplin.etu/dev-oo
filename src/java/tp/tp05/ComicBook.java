package tp.tp05;

import java.time.LocalDate;

/**
 * The type Comic book.
 */
public class ComicBook extends Book{
    private String illustrator;

    /**
     * Instantiates a new Comic book.
     *
     * @param code            the code
     * @param title           the title
     * @param author          the author
     * @param publicationYear the publication year
     * @param illustrator     the illustrator
     */
    public ComicBook(String code, String title, String author, int publicationYear, String illustrator) {
        super(code, title, author, publicationYear);
        this.illustrator = illustrator;
    }

    /**
     * Gets illustrator.
     *
     * @return the illustrator
     */
    public String getIllustrator() {
        return illustrator;
    }

    /**
     * Sets illustrator.
     *
     * @param illustrator the illustrator
     */
    public void setIllustrator(String illustrator) {
        this.illustrator = illustrator;
    }

    @Override
    public String toString () {
        String res = super.toString();
        res = "ComicBook" + res.substring(5, res.length() - 1) + ',' + this.illustrator + ']';
        return res;
    }

    @Override
    public int getDurationMax () {
        int res = super.getDurationMax();
        if ((LocalDate.now().getYear() - this.publicationYear) <= 2) res = res/3;
        return res;
    }
}
