package tp.tp05;

/**
 * The type Use library.
 */
public class UseLibrary {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Library l1 = new Library();
        Book b1 = new Book("H2G2", "The Hitchhiker’s Guide to the Galaxy", "D. Adams", 1979);
        Book b2 = new Book("FLTL", "Flatland", "E.Abbott Abbott", 1884);
        Book b3 = new Book("REU", "The Restaurant at the End of the Universe", "D. Adams", 1980);
        l1.addBook(b1);
        l1.addBook(b2);
        l1.addBook(b3);

        System.out.println(b1 + "\n" + l1 + "\nBorrowings list");
        System.out.println("Initially-->" + l1.borrowings());
        System.out.println(l1.borrow("H2G2", 42) + "--> " + l1.borrowings());
        System.out.println(l1.giveBack("FLTL")  + "--> " + l1.borrowings());
        System.out.println(l1.borrow("REU", 404) + "--> " + l1.borrowings());
        System.out.println(l1.borrow("FLTL", 42) + "--> " + l1.borrowings());
        l1.giveBack("H2G2");
        l1.giveBack("FLTL");
        l1.giveBack("REU");

        Library l2 = new Library();
        ComicBook c1 = new ComicBook("LeuG", "Léonard est un Génie", "Bob de Groot", 1977, "Turk");

        l2.addBook(b1);
        l2.addBook(c1);
        System.out.println(l2);
        System.out.println(l2.borrow("H2G2", 42)  + "--> " + l2.borrowings());
        System.out.println(l2.borrow("LeuG", 42)  + "--> " + l2.borrowings());

    }
}
