package tp.tp10;

import java.util.Comparator;

public class PriorityComparator<T extends IPriority> implements Comparator<T> {
    @Override
    public int compare(T o1, T o2) {
        return o1.getPriority() - o2.getPriority();
    }
}

