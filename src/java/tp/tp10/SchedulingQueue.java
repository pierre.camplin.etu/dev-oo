package tp.tp10;

import java.util.LinkedList;

public class SchedulingQueue<T> implements IScheduler<T>{
    protected LinkedList<T> theQueue;
    public static int queueCounter;
    private int ID;

    public SchedulingQueue() {
        this.theQueue = new LinkedList<>();
        this.ID = queueCounter;
        queueCounter++;
    }

    public int getID () {
        return this.ID;
    }

    @Override
    public String toString() {
        return "Queue" + this.ID + " -> " + theQueue;
    }

    @Override
    public void addElement(T element) {
        this.theQueue.add(element);
    }

    @Override
    public T highestPriority() {
        return this.theQueue.poll();
    }

    @Override
    public boolean isEmpty() {
        return this.theQueue.size() == 0;
    }

    @Override
    public int size() {
        return this.theQueue.size();
    }
}
