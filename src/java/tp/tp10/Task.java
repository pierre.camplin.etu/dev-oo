package tp.tp10;

import org.jetbrains.annotations.NotNull;

public class Task implements IPriority {
    private int priority;
    private String label;

    public Task (String label, int priority) {
        this.label = label;
        this.priority = priority;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Task:" + this.label + " (" + this.priority + ")";
    }
}
