package tp.tp10;

import tp.tp01.Book;

public class UseSchedulingQueue {
    public static void main(String[] args) {
        SchedulingQueue<Book> sqBook = new SchedulingQueue<>();
        SchedulingQueue<Task> sqTask = new SchedulingQueue<>();

        Book b1 = new Book("H2G2", "D. Adams", 1979);
        Book b2 = new Book("Flatland", "E.Abbott Abbott", 1884);
        Book b3 = new Book("4Chan", "Philosoraptor", 2008);

        Task t1 = new Task("a0", 2);
        Task t2 = new Task("a1", 4);
        Task t3 = new Task("a2", 3);

        sqBook.addElement(b1);
        sqBook.addElement(b2);
        sqBook.addElement(b3);
        System.out.println(sqBook);
        sqBook.highestPriority();
        System.out.println(sqBook);
        sqBook.highestPriority();
        System.out.println(sqBook);
        sqBook.highestPriority();
        System.out.println(sqBook);

        sqTask.addElement(t1);
        sqTask.addElement(t2);
        sqTask.addElement(t3);
        System.out.println(sqTask);
        sqTask.highestPriority();
        System.out.println(sqTask);
        sqTask.highestPriority();
        System.out.println(sqTask);
        sqTask.highestPriority();
        System.out.println(sqTask);
    }
}
