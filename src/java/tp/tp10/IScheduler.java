package tp.tp10;

public interface IScheduler<T> {
    void addElement (T element);
    T highestPriority ();
    boolean isEmpty ();

    int size ();
}
