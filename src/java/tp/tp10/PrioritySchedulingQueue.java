package tp.tp10;

import java.util.Collections;

public class PrioritySchedulingQueue<T extends IPriority> extends SchedulingQueue<T> {
    @Override
    public void addElement(T element) {
        super.addElement(element);
        this.theQueue.sort(new PriorityComparator<>());
    }
}
