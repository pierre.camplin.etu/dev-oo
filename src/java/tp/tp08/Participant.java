package tp.tp08;

public class Participant {
    private String name;
    private Participant partner;

    public Participant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Participant getPartner() {
        return partner;
    }

    public String getPartnerName() {
        return this.partner.getName();
    }

    public void matchWith(Participant p2) {
    }

    public boolean isMatchedWith(Participant p2) {
        return false;
    }
}
