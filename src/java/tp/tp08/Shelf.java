package tp.tp08;

import java.util.ArrayList;

public class Shelf {
    private ArrayList<IProduct> shelves;
    private boolean refrigerated;
    private int capacityMax;

    public Shelf (boolean refrigerated, int capacityMax) {
        this.refrigerated = refrigerated;
        this.capacityMax = capacityMax;
        this.shelves = new ArrayList<>();
    }

    public ArrayList<IProduct> getShelves() {
        return shelves;
    }

    public boolean isRefrigerated() {
        return refrigerated;
    }

    public boolean isFull () {
        return this.shelves.size() >= this.capacityMax;
    }

    public boolean isEmpty () {
        return this.shelves.isEmpty();
    }

    @Override
    public String toString () {
        return "[" + this.refrigerated + " : " + this.capacityMax + " -> " + this.shelves + "]";
    }

    public boolean add (IProduct p) {
        if (this.refrigerated == p.isPerishable() && !this.isFull()) {
            return this.shelves.add(p);
        }
        return false;
    }
}
