package tp.tp08;


import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Food implements IProduct, Comparable<Food>{
    private String label;
    private double price;
    private LocalDate bestBeforeDate;
    private static int cptUnknown;

    public Food(String label, double price, LocalDate bestBeforeDate) {
        if (label == null) this.label = "refUnknown" + cptUnknown++;
        else this.label = label;
        this.price = price;
        this.bestBeforeDate = bestBeforeDate;
    }

    public Food(String label, double price) {
        this(label, price, LocalDate.now().plusDays(10));
    }

    public String getLabel () {
        return this.label;
    }
    @Override
    public double getPrice() {
        return this.price;
    }

    public LocalDate getBestBeforeDate() {
        return bestBeforeDate;
    }

    @Override
    public boolean isPerishable() {
        return true;
    }

    public boolean isBestBefore(LocalDate aDate) {
        return aDate.isBefore(this.bestBeforeDate);
    }

    @Override
    public String toString () {
        return "[" + this.label + "=" + this.price + " -> before " + this.bestBeforeDate + "]";
    }

    @Override
    public int compareTo(@NotNull Food o) {
        return (int)o.bestBeforeDate.until(this.bestBeforeDate, ChronoUnit.DAYS);
    }
}
