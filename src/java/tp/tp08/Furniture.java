package tp.tp08;

public class Furniture implements IProduct{
    private String label;
    private double price;

    public Furniture(String label, double price) {
        this.label = label;
        this.price = price;
    }

    public String getLabel() {
        return label;
    }
    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public boolean isPerishable() {
        return false;
    }

    @Override
    public String toString () {
        return "[" + this.label + "=" + this.price + "]";
    }
}
