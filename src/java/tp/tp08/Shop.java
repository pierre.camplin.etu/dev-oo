package tp.tp08;

import java.time.LocalDate;
import java.util.ArrayList;

public class Shop {
    private ArrayList<Shelf> shelving;

    public Shop () {
        this(new ArrayList<>());
    }

    public Shop(ArrayList<Shelf> shelving) {
        this.shelving = shelving;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "shelving=" + shelving +
                '}';
    }

    public ArrayList<Shelf> getShelving() {
        return shelving;
    }

    public void tidy(IProduct product) {
        int i = 0;
        while (!this.shelving.get(i).add(product) && i < this.shelving.size()) i++;
    }

    public void tidy(ArrayList<IProduct> aStock) {
        for (IProduct product : aStock) {
            this.tidy(product);
        }
    }

    public ArrayList<IProduct> closeBestBeforeDate(int nbDays){
        ArrayList<IProduct> res = new ArrayList<IProduct>();
        for(Shelf s: this.shelving){
            for(IProduct p : s.getShelves()){
                if(p.isPerishable()){
                    Food f = (Food) p;
                    if (LocalDate.now().compareTo(f.getBestBeforeDate())>nbDays){
                        res.add(p);
                    }
                }
            }
        }
        return res;
    }
}
