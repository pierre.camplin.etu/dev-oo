package tp.tp04;

import java.util.Arrays;

/**
 * The type Student.
 */
public class Student {
  private double[] grades;
  private Person person;

  private Student(Person pers, double[] grades) {
    this.person = pers;
    this.grades = grades;
  }

    /**
     * Instantiates a new Student.
     *
     * @param id       the id
     * @param name     the name
     * @param forename the forename
     * @param grades   the grades
     */
    public Student(String id, String name, String forename, double[] grades) {
    this(new Person(id, name, forename), grades);
  }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() { return person.getName(); }

    /**
     * Gets forename.
     *
     * @return the forename
     */
    public String getForename() { return person.getForename(); }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() { return person.getId(); }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) { person.setName(name); }

    /**
     * Sets forename.
     *
     * @param forename the forename
     */
    public void setForename(String forename) { person.setForename(forename); }

    /**
     * Gets average.
     *
     * @return the average
     */
    public double getAverage() {
    int s = 0;
    for (double grade : this.grades) {
      s += grade;
    }
    return s / this.grades.length;
  }

    /**
     * Add grade.
     *
     * @param grade the grade
     */
    public void addGrade(double grade) {
    this.grades[this.grades.length - 1] = grade;
  }

    /**
     * Equals boolean.
     *
     * @param other the other
     * @return the boolean
     */
    public boolean equals(Student other) {
    if (this == other) return true;
    if (!this.person.equals(other.person)) return false;
    if (!Arrays.equals(this.grades, other.grades)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "Student " + this.person.getForename() + " " + this.person.getName();
  }
}
