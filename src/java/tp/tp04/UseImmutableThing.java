package tp.tp04;

import java.time.LocalDate;

/**
 * The type Use immutable thing.
 */
public class UseImmutableThing {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
    Point point = new Point(0, 0);
    ImmutableThing thingy = new ImmutableThing(0, "yoyo", LocalDate.now(), point, new int[]{ 1, 2, 3 });
    // We want to check if the point given to `thingy` is going to change
    // if we change the reference we gave it afterwards.
    System.out.println("The point before any change : " + point);
    point.add(5, 5);
    System.out.println("The modified original point : " + point); // should display (5.0;5.0)
    System.out.println("The point of 'thingy' : " + thingy.getOrigin()); // should display (0.0:0.0)
  }
}
