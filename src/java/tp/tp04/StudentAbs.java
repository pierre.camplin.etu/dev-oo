package tp.tp04;

/**
 * The type Student abs.
 */
public class StudentAbs {
  private int nbAbsence;
  private Student etu;

  private StudentAbs(Student student, int nbAbsence) {
    this.etu = student;
    this.nbAbsence = nbAbsence;
  }

    /**
     * Instantiates a new Student abs.
     *
     * @param id        the id
     * @param name      the name
     * @param forename  the forename
     * @param grades    the grades
     * @param nbAbsence the nb absence
     */
    public StudentAbs(String id, String name, String forename, double[] grades, int nbAbsence) {
    this(new Student(id, name, forename, grades), nbAbsence);
  }

    /**
     * Add grade.
     *
     * @param grade the grade
     */
    public void addGrade(double grade) {
    this.etu.addGrade(grade);
  }

    /**
     * Equals boolean.
     *
     * @param student the student
     * @return the boolean
     */
    public boolean equals(StudentAbs student) {
    if (this == student) return true;
    if (this.nbAbsence != student.nbAbsence) return false;
    if (!this.etu.equals(student.etu)) return false;
    return true;
  }

    /**
     * Warning boolean.
     *
     * @param tresholdAbs the treshold abs
     * @param tresholdAvg the treshold avg
     * @return the boolean
     */
    public boolean warning(int tresholdAbs, double tresholdAvg) {
    return this.nbAbsence >= tresholdAbs ? true : this.etu.getAverage() <= tresholdAvg;
  }

    /**
     * Validation boolean.
     *
     * @param tresholdAbs the treshold abs
     * @param tresholdAvg the treshold avg
     * @return the boolean
     */
    public boolean validation(int tresholdAbs, double tresholdAvg) {
    return this.nbAbsence <= tresholdAbs ? true : this.etu.getAverage() >= tresholdAvg;
  }

  @Override
  public String toString() {
    return this.etu.toString();
  }
}
