package tp.tp04;

import java.time.LocalDate;
import java.util.Arrays;

/**
 * The type Immutable thing.
 */
public class ImmutableThing {
  private int number;
  private String ref;
  private LocalDate date;
  private Point origin;
  private int[] numbers;

    /**
     * Instantiates a new Immutable thing.
     *
     * @param numero  the numero
     * @param ref     the ref
     * @param date    the date
     * @param origine the origine
     * @param nombres the nombres
     */
    public ImmutableThing(int numero, String ref, LocalDate date, Point origine, int[] nombres) {
    this.number = numero;
    this.ref = new String(ref);
    this.date = this.copyDate(date);
    this.origin = this.copyOrigin(origine);
    this.numbers = this.copyNumbers(nombres);
  }

  private LocalDate copyDate(LocalDate date) { return LocalDate.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth()); }
  private Point copyOrigin(Point origin) { return new Point(origin.getX(), origin.getY()); }
  private int[] copyNumbers(int[] numbers) { return Arrays.copyOf(numbers, numbers.length); }

    /**
     * Gets number.
     *
     * @return the number
     */
    public int getNumber() { return this.number; }

    /**
     * Gets origin.
     *
     * @return the origin
     */
    public Point getOrigin() { return this.copyOrigin(this.origin); }

    /**
     * Gets reference.
     *
     * @return the reference
     */
    public String getReference() { return new String(ref); }

    /**
     * Gets date.
     *
     * @return the date
     */
    public LocalDate getDate() { return this.copyDate(this.date); }

    /**
     * Get numbers int [ ].
     *
     * @return the int [ ]
     */
    public int[] getNumbers() { return this.copyNumbers(this.numbers); }

  @Override
  public String toString() {
    return "Thing at position " + this.origin;
  }
}
