package tp.tp04;

import tpOO.tp04.PendingCase;
import java.util.Arrays;

/**
 * The type Pending case queue.
 */
public class PendingCaseQueue {
  private PendingCase[] myQueue;
  private int capacityMax;
  private int head = 0;
  private int tail = 0;

    /**
     * Instantiates a new Pending case queue.
     *
     * @param size the size
     */
    public PendingCaseQueue(int size) {
    this.capacityMax = size;
    this.myQueue = new PendingCase[size];
  }

    /**
     * Clear.
     */
    public void clear() {
    Arrays.fill(this.myQueue, null);
  }

    /**
     * Gets pending case.
     *
     * @param i the
     * @return the pending case
     */
    public PendingCase getPendingCase(int i) {
    if (i >= this.capacityMax || i < 0) {
      return null;
    }
    return this.myQueue[i];
  }

    /**
     * Is empty boolean.
     *
     * @return the boolean
     */
    public boolean isEmpty() {
    for (PendingCase pendingCase : this.myQueue) {
      if (pendingCase != null) {
        return false;
      }
    }
    return true;
  }

    /**
     * Is full boolean.
     *
     * @return the boolean
     */
    public boolean isFull() {
    for (PendingCase pendingCase : this.myQueue) {
      if (pendingCase == null) {
        return false;
      }
    }
    return true;
  }

    /**
     * Index increment int.
     *
     * @param idx the idx
     * @return the int
     */
    public int indexIncrement(int idx) {
    return (idx + 1) % this.capacityMax;
  }

    /**
     * Add one boolean.
     *
     * @param anotherPendingCase the another pending case
     * @return the boolean
     */
    public boolean addOne(PendingCase anotherPendingCase) {
    if (this.myQueue[this.tail] != null) {
      return false;
    }
    this.myQueue[this.tail] = anotherPendingCase;
    this.tail = indexIncrement(this.tail);
    return true;
  }

    /**
     * Size int.
     *
     * @return the int
     */
    public int size() {
    int n = 0;
    for (PendingCase p : this.myQueue) {
      if (p != null) {
        ++n;
      }
    }
    return n;
  }

    /**
     * Remove one pending case.
     *
     * @return the pending case
     */
    public PendingCase removeOne() {
    this.myQueue[this.head] = null;
    this.head = indexIncrement(this.head);
    return this.myQueue[this.head];
  }

    /**
     * Gets total amount.
     *
     * @return the total amount
     */
    public double getTotalAmount() {
    int amount = 0;
    for (PendingCase p : this.myQueue) {
      if (p != null) {
        amount += p.getAmount();
      }
    }
    return amount;
  }

    /**
     * Cheating.
     *
     * @param anotherPendingCase the another pending case
     * @param position           the position
     */
    public void cheating(PendingCase anotherPendingCase, int position) {
    if (position >= this.capacityMax || position < 0 || isFull()) return;
    if (this.myQueue[position] == null) {
      this.myQueue[position] = anotherPendingCase;
    } else {
      PendingCase[] copyQueue = new PendingCase[this.capacityMax];
      for (int i = 0; i < position; ++i) {
        copyQueue[i] = this.myQueue[i];
      }
      for (int i = position; i < this.capacityMax; ++i) {
        if (this.myQueue[i] == null) {
          break;
        }
        copyQueue[(i + 1) % this.capacityMax] = this.myQueue[i];
      }
      this.myQueue = copyQueue;
      this.myQueue[position] = anotherPendingCase;
    }
  }

  @Override
  public String toString() {
    String res = "";
    for (PendingCase p : this.myQueue) {
      res += "\n  " + p;
    }
    return res;
  }
}