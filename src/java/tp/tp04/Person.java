package tp.tp04;

/**
 * The type Person.
 */
public class Person {
  private String id;
  private String name;
  private String forename;

    /**
     * Instantiates a new Person.
     *
     * @param id       the id
     * @param name     the name
     * @param forename the forename
     */
    public Person(String id, String name, String forename) {
    this.id = id;
    this.name = name;
    this.forename = forename;
  }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() { return name; }

    /**
     * Gets forename.
     *
     * @return the forename
     */
    public String getForename() { return forename; }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() { return id; }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) { this.name = name; }

    /**
     * Sets forename.
     *
     * @param forename the forename
     */
    public void setForename(String forename) { this.forename = forename; }

    /**
     * Equals boolean.
     *
     * @param other the other
     * @return the boolean
     */
    public boolean equals(Person other) {
    if (this == other) return true;
    if (this.id != other.id) return false;
    return true;
  }

  @Override
  public String toString() {
    return forename + " " + name;
  }
}
