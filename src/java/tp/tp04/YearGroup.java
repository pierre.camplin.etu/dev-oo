package tp.tp04;

/**
 * The type Year group.
 */
public class YearGroup {
  private StudentAbs[] yg;

    /**
     * Instantiates a new Year group.
     *
     * @param students the students
     */
    public YearGroup(StudentAbs[] students) {
    this.yg = students;
  }

    /**
     * Get group student abs [ ].
     *
     * @return the student abs [ ]
     */
    public StudentAbs[] getGroup() { return this.yg; }

    /**
     * Sets group.
     *
     * @param group the group
     */
    public void setGroup(StudentAbs[] group) { this.yg = group; }

    /**
     * Add grades.
     *
     * @param grades the grades
     */
    public void addGrades(double[] grades) {
    for (int i = 0; i < this.yg.length; ++i) {
      this.yg[i].addGrade(grades[i]);
    }
  }

    /**
     * Validation.
     *
     * @param thresholdAbs the threshold abs
     * @param thresholdAvg the threshold avg
     */
    public void validation(int thresholdAbs, int thresholdAvg) {
    for (StudentAbs student : this.yg) {
      if (student.validation(thresholdAbs, thresholdAvg)) {
        System.out.println(student + " réussit son année.");
      }
    }
  }
}
