package tp.tp04;

/**
 * The type Point.
 */
public class Point {
  private double x;
  private double y;

    /**
     * Instantiates a new Point.
     *
     * @param x the x
     * @param y the y
     */
    public Point(double x, double y) {
    this.x = x;
    this.y = y;
  }

    /**
     * Add.
     *
     * @param x the x
     * @param y the y
     */
    public void add(double x, double y) {
    this.x += x;
    this.y += y;
  }

    /**
     * Gets x.
     *
     * @return the x
     */
    public double getX() { return this.x; }

    /**
     * Gets y.
     *
     * @return the y
     */
    public double getY() { return this.y; }

  @Override
  public String toString() {
    return "(" + this.x + ";" + this.y + ")";
  }
}
