package tp.tp03;

/**
 * The enum Task status.
 */
public enum TaskStatus {
    /**
     * Todo task status.
     */
    TODO,
    /**
     * Ongoing task status.
     */
    ONGOING,
    /**
     * Delayed task status.
     */
    DELAYED,
    /**
     * Finished task status.
     */
    FINISHED;
}
