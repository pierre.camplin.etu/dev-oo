package tp.tp03;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import util.Keyboard;

/**
 * The type Use local date.
 */
public class UseLocalDate {
    /**
     * Date of the day string.
     *
     * @return the string
     */
    static String dateOfTheDay () {
        return "Today's date is " + LocalDate.now();
    }

    /**
     * Input date local date.
     *
     * @return the local date
     */
    static LocalDate inputDate() {
        int day = Keyboard.readInt("Quel jour ? ");
        int month = Keyboard.readInt("Quel mois ? ");
        int year = Keyboard.readInt("Quel année ? ");
        return LocalDate.of(year, month, day);
    }

    /**
     * Input date local date.
     *
     * @param message the message
     * @return the local date
     */
    static LocalDate inputDate(String message) {
        System.out.println(message);
        int day = Keyboard.readInt("Quel jour ? ");
        int month = Keyboard.readInt("Quel mois ? ");
        int year = Keyboard.readInt("Quel année ? ");
        return LocalDate.of(year, month, day);
    }

    /**
     * Diff date string.
     *
     * @return the string
     */
    static String diffDate () {
        LocalDate date1 = inputDate("Quel est la première date ? ");
        LocalDate date2 = inputDate("Quel est la seconde date ? ");
        long difference = date1.until(date2, ChronoUnit.DAYS);
        return "Il y a un écart de " + difference + " jours entre le " + date1 + " et le " + date2;

    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        System.out.println(UseLocalDate.diffDate());
    }
}
