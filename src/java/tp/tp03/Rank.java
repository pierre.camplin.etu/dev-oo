package tp.tp03;

/**
 * The enum Rank.
 */
public enum Rank {
    /**
     * Seven rank.
     */
    SEVEN,
    /**
     * Eight rank.
     */
    EIGHT,
    /**
     * Nine rank.
     */
    NINE,
    /**
     * Ten rank.
     */
    TEN,
    /**
     * Jack rank.
     */
    JACK,
    /**
     * Queen rank.
     */
    QUEEN,
    /**
     * King rank.
     */
    KING,
    /**
     * Ace rank.
     */
    ACE;
}
