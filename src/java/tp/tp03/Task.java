package tp.tp03;

import java.time.LocalDate;

/**
 * The type Task.
 */
public class Task {
    private String id = generateId();
    private LocalDate creationDate;
    private LocalDate deadline;
    private TaskStatus state;
    private String description;
    private int duration;
    private static int numAuto = 1;

    private static String generateId() {
        String res = "";
        String currentYear = Integer.toString(LocalDate.now().getYear());
        String currentMonth = Integer.toString(LocalDate.now().getMonthValue());
        res = currentYear.substring(2, currentYear.length()) + currentMonth + numAuto++;
        return res;
    }

    /**
     * Instantiates a new Task.
     *
     * @param description the description
     * @param duration    the duration
     */
    public Task(String description, int duration) {
        this.description = description;
        this.creationDate = LocalDate.now();
        this.state = TaskStatus.TODO;
        this.deadline = LocalDate.now().plusDays(10L);
        this.duration = duration;
    }

    /**
     * Instantiates a new Task.
     *
     * @param description the description
     * @param creation    the creation
     * @param deadline    the deadline
     * @param duration    the duration
     */
    public Task(String description, LocalDate creation, LocalDate deadline, int duration) {
        this.description = description;
        this.creationDate = creation;
        this.state = TaskStatus.TODO;
        this.deadline = deadline;
        this.duration = duration;
    }

    /**
     * Change status.
     */
    public void changeStatus() {
        this.state = TaskStatus.values()[(this.state.ordinal() + 1) % TaskStatus.values().length];
    }

    /**
     * Change status.
     *
     * @param ts the ts
     */
    public void changeStatus(TaskStatus ts) {
        this.state = ts;
    }

    /**
     * Change status.
     *
     * @param c the c
     */
    public void changeStatus(char c) {
        switch (Character.toUpperCase(c)) {
            case 'D':
                this.state = TaskStatus.DELAYED;
                break;
            case 'F':
                this.state = TaskStatus.FINISHED;
                break;
            case 'O':
                this.state = TaskStatus.ONGOING;
                break;
            case 'T':
                this.state = TaskStatus.TODO;
        }

    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Gets creation.
     *
     * @return the creation
     */
    public LocalDate getCreation() {
        return this.creationDate;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    public TaskStatus getState() {
        return this.state;
    }

    /**
     * Gets deadline.
     *
     * @return the deadline
     */
    public LocalDate getDeadline() {
        return this.deadline;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Gets duration.
     *
     * @return the duration
     */
    public int getDuration() {
        return this.duration;
    }

    public String toString() {
        return "(T" + this.id + " = " + this.description + ":" + this.state + "(" + this.duration + "):" + this.deadline + ")";
    }

    /**
     * Is late boolean.
     *
     * @return the boolean
     */
    public boolean isLate() {
        return this.deadline.isBefore(LocalDate.now());
    }

    /**
     * Delay.
     *
     * @param nbDays the nb days
     */
    public void delay(int nbDays) {
        this.deadline.plusDays((long)nbDays);
    }

    /**
     * Equals boolean.
     *
     * @param other the other
     * @return the boolean
     */
    public boolean equals(Task other) {
        if (other == null) {
            return false;
        } else {
            if (this.creationDate == null) {
                if (other.creationDate != null) {
                    return false;
                }
            } else if (!this.creationDate.equals(other.creationDate)) {
                return false;
            }

            if (this.deadline == null) {
                if (other.deadline != null) {
                    return false;
                }
            } else if (!this.deadline.equals(other.deadline)) {
                return false;
            }

            if (this.description == null) {
                if (other.description != null) {
                    return false;
                }
            } else if (!this.description.equals(other.description)) {
                return false;
            }

            return this.state == other.state;
        }
    }

    /**
     * Compare deadline int.
     *
     * @param t1 the t 1
     * @param t2 the t 2
     * @return the int
     */
    public static int compareDeadline(Task t1, Task t2) {
        if (t1 == null && t2 == null) {
            return 0;
        } else if (t1 == null && t2 != null) {
            return -1;
        } else {
            return t2 == null && t1 != null ? 1 : t1.getDeadline().compareTo(t2.getDeadline());
        }
    }

    /**
     * Compare duration int.
     *
     * @param t1 the t 1
     * @param t2 the t 2
     * @return the int
     */
    public static int compareDuration(Task t1, Task t2) {
        if (t1 == null && t2 == null) {
            return 0;
        } else if (t1 == null && t2 != null) {
            return -1;
        } else {
            return t2 == null && t1 != null ? 1 : t1.getDuration() - t2.getDuration();
        }
    }

    /**
     * Compare creation int.
     *
     * @param t1 the t 1
     * @param t2 the t 2
     * @return the int
     */
    public static int compareCreation(Task t1, Task t2) {
        if (t1 == null && t2 == null) {
            return 0;
        } else if (t1 == null && t2 != null) {
            return -1;
        } else {
            return t2 == null && t1 != null ? 1 : t1.getCreation().compareTo(t2.getCreation());
        }
    }
}
