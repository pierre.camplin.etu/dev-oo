package tp.tp03;

/**
 * The type Card.
 */
public class Card {
    private Color color;
    private Rank rank;

    /**
     * Instantiates a new Card.
     *
     * @param color the color
     * @param rank  the rank
     */
    public Card (Color color, Rank rank) {
        this.color = color;
        this.rank = rank;
    }

    /**
     * Instantiates a new Card.
     *
     * @param color the color
     * @param rank  the rank
     */
    public Card (String color, String rank) {
        this.color = Color.valueOf(color);
        this.rank = Rank.valueOf(rank);
    }

    /**
     * Gets color.
     *
     * @return the color
     */
    public Color getColor () {return this.color;}

    /**
     * Gets rank.
     *
     * @return the rank
     */
    public Rank getRank () {return this.rank;}

    /**
     * Compare rank int.
     *
     * @param c the c
     * @return the int
     */
    public int compareRank (Card c) {
        if (c != null && this.rank != null && c.rank != null) return this.rank.ordinal() - c.rank.ordinal();
        else return -1;
    }

    /**
     * Compare color int.
     *
     * @param c the c
     * @return the int
     */
    public int compareColor (Card c) {
        if (c != null && this.color != null && c.color != null) return this.color.ordinal() - c.color.ordinal();
        else return -1;
    }

    /**
     * Is before boolean.
     *
     * @param c the c
     * @return the boolean
     */
    public boolean isBefore (Card c) {return this.compareColor(c) < 0;}

    /**
     * Equals boolean.
     *
     * @param c the c
     * @return the boolean
     */
    public boolean equals (Card c) { return this.compareColor(c) == 0 && this.compareRank(c) == 0;}
    public String toString () {return this.rank.toString() + " " + this.color.toString();}
}
