package tp.tp03;

import java.time.LocalDate;
import java.util.Arrays;

/**
 * The type To do list.
 */
public class ToDoList {
    private Task[] chores = new Task[5];

    /**
     * Instantiates a new To do list.
     */
    public ToDoList() {
    }

    /**
     * Enlarge.
     */
    public void enlarge() {
        this.chores = (Task[])Arrays.copyOf(this.chores, this.chores.length + 5);
    }

    /**
     * Add task.
     *
     * @param aTask the a task
     */
    public void addTask(Task aTask) {
        int i;
        for(i = 0; i < this.chores.length && this.chores[i] != null; ++i) {
        }

        if (i < this.chores.length) {
            this.chores[i] = aTask;
        } else {
            this.enlarge();
            this.chores[i] = aTask;
        }

    }

    /**
     * Remove task.
     *
     * @param aTask the a task
     */
    public void removeTask(Task aTask) {
        int i;
        for(i = 0; i < this.chores.length && !this.chores[i].equals(aTask); ++i) {
        }

        if (i < this.chores.length) {
            this.chores[i] = null;
        }

    }

    /**
     * Remove task.
     *
     * @param i the
     */
    public void removeTask(int i) {
        if (i < this.chores.length) {
            this.chores[i] = null;
        }

    }

    /**
     * Is overwhelmed boolean.
     *
     * @return the boolean
     */
    public boolean isOverwhelmed() {
        int i;
        for(i = 0; i < this.chores.length && this.chores[i] != null; ++i) {
        }

        return i < this.chores.length;
    }

    /**
     * Gets nb tasks.
     *
     * @return the nb tasks
     */
    public int getNbTasks() {
        int res = 0;

        for(int i = 0; i < this.chores.length; ++i) {
            if (this.chores[i] != null) {
                ++res;
            }
        }

        return res;
    }

    /**
     * On sick leave.
     *
     * @param nbDays the nb days
     */
    public void onSickLeave(int nbDays) {
        for(int i = 0; i < this.chores.length; ++i) {
            if (this.chores[i] != null) {
                this.chores[i].delay(nbDays);
            }
        }

    }

    /**
     * Due tasks task [ ].
     *
     * @return the task [ ]
     */
    public Task[] dueTasks() {
        int nb = 0;
        Task[] res = new Task[this.chores.length];

        for(int i = 0; i < this.chores.length; ++i) {
            if (this.chores[i].getDeadline().equals(LocalDate.now())) {
                res[nb++] = this.chores[i];
            }
        }

        return (Task[])Arrays.copyOfRange(this.chores, 0, nb);
    }

    /**
     * Emergency sort.
     */
    public void emergencySort() {
        for(int i = 0; i < this.chores.length - 1; ++i) {
            for(int j = i + 1; j < this.chores.length; ++j) {
                if (Task.compareDeadline(this.chores[i], this.chores[j]) < 0) {
                    Task tmp = this.chores[i];
                    this.chores[i] = this.chores[j];
                    this.chores[j] = tmp;
                }
            }
        }

    }

    /**
     * Duration sort.
     */
    public void durationSort() {
        for(int i = 0; i < this.chores.length - 1; ++i) {
            for(int j = i + 1; j < this.chores.length; ++j) {
                if (Task.compareDuration(this.chores[i], this.chores[j]) < 0) {
                    Task tmp = this.chores[i];
                    this.chores[i] = this.chores[j];
                    this.chores[j] = tmp;
                }
            }
        }

    }

    /**
     * Seniority sort.
     */
    public void senioritySort() {
        for(int i = 0; i < this.chores.length - 1; ++i) {
            for(int j = i + 1; j < this.chores.length; ++j) {
                if (Task.compareCreation(this.chores[i], this.chores[j]) < 0) {
                    Task tmp = this.chores[i];
                    this.chores[i] = this.chores[j];
                    this.chores[j] = tmp;
                }
            }
        }

    }

    public String toString() {
        return Arrays.deepToString(this.chores);
    }

    /**
     * Get chores task [ ].
     *
     * @return the task [ ]
     */
    public Task[] getChores() {
        return this.chores;
    }
}
