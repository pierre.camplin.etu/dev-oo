package tp.tp03;

/**
 * The enum Color.
 */
public enum Color {
    /**
     * Club color.
     */
    CLUB,
    /**
     * Diamond color.
     */
    DIAMOND,
    /**
     * Heart color.
     */
    HEART,
    /**
     * Spade color.
     */
    SPADE;
}
