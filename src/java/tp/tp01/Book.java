package tp.tp01;

/**
 * The type Book.
 */
public class Book {
    /**
     * The Author.
     */
// class attributes
    private String author;
    /**
     * The Title.
     */
    private String title;
    /**
     * The Year.
     */
    private int year;

    /**
     * Instantiates a new Book.
     *
     * @param author the author
     * @param title  the title
     * @param year   the year
     */
// constructor
    public Book(String author, String title, int year) {
        this.author = author;
        this.title = title;
        this.year = year;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
// methods
    String getAuthor() {
        return this.author;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    String getTitle() {
        return this.title;
    }

    public int getYear() {
        return year;
    }

    public String toString() {
        return this.title + ":" + this.author + "(" + this.year + ")";
    }
}
