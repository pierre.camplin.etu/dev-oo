package tp.tp01;

import java.util.Random;

/**
 * The type Irregular.
 */
public class Irregular {
    /**
     * The Tab.
     */
    int[][] tab;

    /**
     * Instantiates a new Irregular.
     *
     * @param lineSize the line size
     */
    Irregular(int[] lineSize) {
        int[][] tab = new int[lineSize.length][1];
        for (int i = 0; i < lineSize.length; i++) {
            tab[i] = new int[lineSize[i]];
        }
        this.tab = tab;
    }

    /**
     * Random filling.
     */
    void randomFilling() {
        Random rd = new Random();
        for (int i = 0; i < this.tab.length; i++) {
            for (int j = 0; j < this.tab[i].length; j++) {
                this.tab[i][j] = rd.nextInt();
            }
        }
    }

    /**
     * Display string.
     *
     * @return the string
     */
    String display() {
        String res = "";
        for (int i = 0; i < this.tab.length; i++) {
            for (int j = 0; j < this.tab[i].length; j++) {
                res += this.tab[i][j] + "; ";
            }
            res += "\n";
        }
        return res;
    }

    /**
     * Is common boolean.
     *
     * @param element the element
     * @return the boolean
     */
    boolean isCommon (int element) {
        boolean present = false;
        int j;
        for (int i = 0; i < this.tab.length; i++) {
            present = false;
            j = 0;
            while (!present && j < this.tab[i].length) {
                if (this.tab[i][j] == element) {
                    present = true;
                }
                j++;
            }
            if (!present) {
                break;
            }
        }
        return present;
    }

    /**
     * Exist common boolean.
     *
     * @return the boolean
     */
    boolean existCommon () {
        boolean present = false;
        int i = 0;
        while (!present && i < this.tab[0].length){
            present = present || this.isCommon(this.tab[0][i]);
            i++;
        }
        return present;
    }
}