package tp.tp01;

/**
 * The type Use book.
 */
public class UseBook {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Book[] biblio = new Book[]{new Book("Edwin A. Abbott", "Flatland", 1884),
                new Book("Risitas", "Issou", 2015),
                new Book("4Chan", "Philosoraptor", 2008)};
        for (Book book : biblio) {
            System.out.println(book.getAuthor() + " a écrit " + book.getTitle() + " en " + book.getYear());
        }
    }
}
