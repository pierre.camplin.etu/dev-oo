package tp.tp01;

import util.HW;

/**
 * The type Use hw.
 */
public class UseHW {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        HW truc = new HW();
        truc.getMessage();
    }
}
