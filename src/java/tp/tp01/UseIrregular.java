package tp.tp01;

/**
 * The type Use irregular.
 */
public class UseIrregular {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Irregular truc = new Irregular(new int[]{3, 4, 2});
        truc.randomFilling();
        System.out.println(truc.display() + truc.existCommon());
    }
}
