package tp.tp01;

import java.util.Random;

/**
 * The type Random sequence.
 */
public class RandomSequence {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Random rd = new Random();
        int nbElt = Integer.parseInt(args[0]);
        int maxVal = Integer.parseInt(args[1]);
        String type;
        if (args.length < 3) {
            type = "INTEGER";
        } else {
            type = args[2];
        }
        if (!(type.equals("INTEGER") || type.equals("REAL"))) {
            System.out.println("Correct usage : <nbElt> <maxVal> [INTEGER|REAL]");
        } else {
            for (int i = 0; i < nbElt; i++) {
                if (type.equals("INTEGER")) {
                    System.out.println(rd.nextInt(maxVal));
                } else {
                    System.out.println(rd.nextDouble() * Double.valueOf(maxVal));
                }
            }
        }
    }
}
