package tp.tp01;

import util.SecretWord;

/**
 * The type Use secret word.
 */
class UseSecretWord {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SecretWord s = new SecretWord();
        System.out.println(s.getSecretWord());
        System.out.println(s.getSecretWord());
        System.out.println(s.getSecretWord());
    }
}
