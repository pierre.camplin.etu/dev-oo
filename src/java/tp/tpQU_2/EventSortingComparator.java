package tp.tpQU_2;

import java.util.Comparator;

/**
 * The type Event sorting comparator.
 */
public class EventSortingComparator implements Comparator<Event> {
    @Override
    public int compare(Event evt1, Event evt2) {
        return evt1.getStart().compareTo(evt2.getStart());
    }
}
