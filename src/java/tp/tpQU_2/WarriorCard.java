package tp.tpQU_2;

import java.util.Objects;

/**
 * The type Warrior card.
 */
public class WarriorCard {
    private String name;
    private int strength;
    private int agility;

    /**
     * Instantiates a new Warrior card.
     *
     * @param name the name
     * @param s    the s
     * @param ag   the ag
     */
    public WarriorCard (String name, int s, int ag) {
        this.name = name;
        this.strength = s;
        this.agility = ag;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof WarriorCard)) return false;
        WarriorCard that = (WarriorCard) obj;
        return Objects.equals(name, that.name);
    }

    /**
     * Compare strength int.
     *
     * @param other the other
     * @return the int
     */
    public int compareStrength (WarriorCard other) {return this.strength - other.strength;}

    /**
     * Compare agility int.
     *
     * @param other the other
     * @return the int
     */
    public int compareAgility (WarriorCard other) {return this.agility - other.agility;}

    @Override
    public String toString() {return this.name + "[S=" + this.strength + ",A=" + this.agility + "]";}
}
