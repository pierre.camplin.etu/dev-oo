package tp.tpQU_2;

import java.util.ArrayList;

/**
 * The type Agenda.
 */
public class Agenda {
    /**
     * The Events.
     */
    public ArrayList<Event> events;

    /**
     * Instantiates a new Agenda.
     */
    public Agenda () {
        this.events = new ArrayList<>();
    }

    @Override
    public String toString() {
        String result= "";
        for (Event event : this.events) {
            if (event != null) result += event + "\n";
        }
        return result;
    }

    /**
     * Conflicting boolean.
     *
     * @param evt the evt
     * @return the boolean
     */
    public boolean conflicting (Event evt) {
        if (this.events.size() == 0) return true;
        boolean result = false;
        for (Event e:events) {
            result = result || e.overlap(evt);
        }
        return !result;
    }

    /**
     * Add event.
     *
     * @param evt the evt
     */
    public void addEvent(Event evt) {
        this.events.add(evt);
        this.sortAgenda();
    }

    /**
     * Add event.
     *
     * @param evt the evt
     * @param idx the idx
     */
    public void addEvent (Event evt, int idx) {
        if (this.events.get(idx) == null) this.events.add(idx, evt);
        this.sortAgenda();
    }

    /**
     * Remove event.
     *
     * @param evt the evt
     */
    public void removeEvent (Event evt) {
        this.events.remove(evt);
    }

    /**
     * Remove event.
     *
     * @param idx the idx
     */
    public void removeEvent (int idx) {
        this.events.remove(idx);
    }

    /**
     * Remove event.
     *
     * @param label the label
     */
    public void removeEvent (String label) {
        boolean trouve = false;
        int i = 0;
        while (!trouve && i < this.events.size()) {
            if (this.events.get(i).getLabel().equals(label)) {
                trouve = true;
                this.removeEvent(i);
            }
            i++;
        }
    }

    /**
     * Remove overlapping.
     *
     * @param evt the evt
     */
    public void removeOverlapping (Event evt) {
        ArrayList<Event> tmp = new ArrayList<>();
        for (Event e:events) {
            if (e.overlap(evt)) tmp.add(e);
        }
        this.events.removeAll(tmp);
    }

    /**
     * Remove between.
     *
     * @param evt1 the evt 1
     * @param evt2 the evt 2
     */
    public void removeBetween (Event evt1, Event evt2) {
        ArrayList<Event> tmp = new ArrayList<>();
        for (Event e:events) {
            if (e.between(evt1, evt2)) tmp.add(e);
        }
        this.events.removeAll(tmp);
    }

    /**
     * Clear.
     */
    public void clear () {
        this.events.clear();
    }

    /**
     * Sort agenda.
     */
    public void sortAgenda () {
        this.events.sort(new EventSortingComparator());
    }
}