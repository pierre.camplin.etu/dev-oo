package tp.tpQU_2;

import tp.tp03.Task;
import tp.tp03.ToDoList;

import java.time.LocalDate;

/**
 * The type Event.
 */
public class Event {
    private String label;
    private String place;
    private LocalDate start;
    private LocalDate end;
    private ToDoList tasks;

    /**
     * Instantiates a new Event.
     *
     * @param label the label
     * @param place the place
     * @param start the start
     * @param end   the end
     * @param tasks the tasks
     */
    public Event(String label, String place, LocalDate start, LocalDate end, ToDoList tasks) {
        this.label = label;
        this.place = place;
        this.start = start;
        if (start.isBefore(end)) this.end = end;
        else this.end = this.start;
        this.tasks = tasks;
    }

    /**
     * Instantiates a new Event.
     *
     * @param label the label
     * @param place the place
     * @param start the start
     * @param end   the end
     */
    public Event(String label, String place, LocalDate start, LocalDate end) {
        this.label = label;
        this.place = place;
        this.start = start;
        if (start.isBefore(end)) this.end = end;
        else this.end = this.start;
        this.tasks = new ToDoList();
    }

    /**
     * Instantiates a new Event.
     *
     * @param label the label
     * @param place the place
     * @param start the start
     */
    public Event(String label, String place, LocalDate start) {
        this.label = label;
        this.place = place;
        this.start = start;
        this.end = this.start.plusDays(1);
        this.tasks = new ToDoList();
    }

    /**
     * Instantiates a new Event.
     *
     * @param label the label
     * @param place the place
     */
    public Event(String label, String place) {
        this.label = label;
        this.place = place;
        this.start = LocalDate.now();
        this.end = this.start.plusDays(1);
        this.tasks = new ToDoList();
    }

    @Override
    public String toString() {
        return this.label + " - " + this.place + ": \t " + this.start + " -> " + this.end;
    }

    /**
     * Gets nb tasks.
     *
     * @return the nb tasks
     */
    public int getNbTasks () {
        return this.tasks.getNbTasks();
    }

    /**
     * Get chores task [ ].
     *
     * @return the task [ ]
     */
    public Task[] getChores () {
        return this.tasks.getChores();
    }

    /**
     * Add task.
     *
     * @param t the t
     */
    public void addTask (Task t) {
        this.tasks.addTask(t);
    }

    /**
     * Gets label.
     *
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets label.
     *
     * @param label the label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Gets place.
     *
     * @return the place
     */
    public String getPlace() {
        return place;
    }

    /**
     * Sets place.
     *
     * @param place the place
     */
    public void setPlace(String place) {
        this.place = place;
    }

    /**
     * Gets start.
     *
     * @return the start
     */
    public LocalDate getStart() {
        return start;
    }

    /**
     * Sets start.
     *
     * @param start the start
     */
    public void setStart(LocalDate start) {
        this.start = start;
    }

    /**
     * Gets end.
     *
     * @return the end
     */
    public LocalDate getEnd() {
        return end;
    }

    /**
     * Sets end.
     *
     * @param end the end
     */
    public void setEnd(LocalDate end) {
        this.end = end;
    }

    /**
     * Gets tasks.
     *
     * @return the tasks
     */
    public ToDoList getTasks() {
        return tasks;
    }

    /**
     * Sets tasks.
     *
     * @param tasks the tasks
     */
    public void setTasks(ToDoList tasks) {
        this.tasks = tasks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;

        Event event = (Event) o;

        if (!this.label.equals(event.label)) return false;
        if (!this.place.equals(event.place)) return false;
        if (!this.start.equals(event.start)) return false;
        return this.end.equals(event.end);
    }

    /**
     * Overlap boolean.
     *
     * @param evt the evt
     * @return the boolean
     */
    public boolean overlap (Event evt) {
        return (this.start.isAfter(evt.start) && this.start.isBefore(evt.end)) ||
                (this.end.isBefore(evt.end) && this.end.isAfter(evt.start)) ||
                (this.start.equals(evt.start)) ||
                (this.end.equals(evt.end));
    }

    /**
     * Between boolean.
     *
     * @param evt1 the evt 1
     * @param evt2 the evt 2
     * @return the boolean
     */
    public boolean between (Event evt1, Event evt2) {
        return (this.start.isAfter(evt1.start) && this.end.isBefore(evt2.end) && !(this.equals(evt1) && this.equals(evt2))) || this.start.isAfter(evt2.start) && this.end.isBefore(evt1.end) && !(this.equals(evt1) && this.equals(evt2));
    }
}
