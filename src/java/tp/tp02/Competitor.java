package tp.tp02;

/**
 * The type Competitor.
 */
public class Competitor {
    private String numberSign;
    private int time;
    private int score;

    /**
     * Instantiates a new Competitor.
     *
     * @param numberSign the number sign
     * @param score      the score
     * @param min        the min
     * @param sec        the sec
     */
    Competitor(int numberSign, int score, int min, int sec) {
        this.numberSign = (numberSign > 0 && numberSign <= 100) ? "No" + numberSign : null;
        this.time = ((min >= 0 && min <= 60) && (sec >= 0 && sec <= 60)) ? min*60 + sec : -1;
        this.score = (score >= 0 && score <= 50) ? score : -1;
    }

    /**
     * Display string.
     *
     * @return the string
     */
    String display () {
        String res = "[";
        if (this.numberSign != null && this.time >= 0 && this.score >= 0) {
            res += this.numberSign + ", " + this.score + " point(s), " + this.time + " s]";
        } else {
            res += "<invalide>, " + this.score + " point(s), " + this.time + " s]";
        }
        return res;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Competitor[] tab = new Competitor[100];
        tab[0] = new Competitor(1, 45, 15, 20);
        tab[1] = new Competitor(2, 32, 12, 45);
        tab[2] = new Competitor(5, 12, 13, 59);
        tab[3] = new Competitor(12, 12, 15, 70);
        tab[4] = new Competitor(32, 75, 15, 20);
        int i = 0;
        while (tab[i] != null && i < tab.length) {
            if (!tab[i].display().startsWith("invalide", 2)) {
                System.out.println(tab[i].display());
            }
            i++;
        }
    }
}
