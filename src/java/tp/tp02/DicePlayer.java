package tp.tp02;

/**
 * The type Dice player.
 */
public class DicePlayer {
    /**
     * The Name.
     */
    String name;
    /**
     * The Total value.
     */
    int totalValue,
    /**
     * The Nb dice rolls.
     */
    nbDiceRolls;

    /**
     * Instantiates a new Dice player.
     *
     * @param name the name
     */
    DicePlayer (String name) {
        this.name = name;
        this.totalValue = 0;
        this.nbDiceRolls = 0;
    }

    /**
     * Play.
     *
     * @param dice6 the dice 6
     */
    void play(Dice dice6) {
        dice6.roll();
        this.totalValue += dice6.value;
        this.nbDiceRolls++;
    }

    public String toString () {
        return this.name + ": " + this.totalValue + " points en " + this.nbDiceRolls + " coups.";
    }
}
