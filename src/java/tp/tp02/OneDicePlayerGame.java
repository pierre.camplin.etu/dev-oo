package tp.tp02;

/**
 * The type One dice player game.
 */
public class OneDicePlayerGame {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        DicePlayer player = new DicePlayer("Joueur");
        Dice dice6 = new Dice(6);
        int i = 0;
        while (player.totalValue < 20) player.play(dice6);
        System.out.println(player);
    }
}
