package tp.tp02;

/**
 * The type Two dice player game.
 */
public class TwoDicePlayerGame {

    /**
     * The Player 1.
     */
    DicePlayer player1,
    /**
     * The Player 2.
     */
    player2;

    /**
     * Instantiates a new Two dice player game.
     *
     * @param player1 the player 1
     * @param player2 the player 2
     */
    TwoDicePlayerGame(DicePlayer player1, DicePlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    /**
     * Winner dice player.
     *
     * @return the dice player
     */
    DicePlayer winner () {
        if (this.player2.totalValue >= 20 && this.player2.nbDiceRolls < this.player1.nbDiceRolls) return player2;
        else return player1;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Dice dice = new Dice(6);
        TwoDicePlayerGame jeu = new TwoDicePlayerGame(new DicePlayer("Joueur 1"), new DicePlayer("Joueur 2"));
        while (jeu.player1.totalValue < 20) jeu.player1.play(dice);
        while (jeu.player2.totalValue < 20) jeu.player2.play(dice);
        System.out.println("Le gagnant est " + jeu.winner());
    }
}
