package tp.tp02;

import util.Keyboard;

/**
 * The type N dice player game.
 */
public class NDicePlayerGame {
    /**
     * The Players.
     */
    DicePlayer[] players;

    /**
     * Instantiates a new N dice player game.
     *
     * @param nbPlayer the nb player
     */
    NDicePlayerGame(int nbPlayer){
        this.players = new DicePlayer[nbPlayer];
        for (int i = 0; i < this.players.length; i++) {
            this.players[i] = new DicePlayer(String.valueOf(i));
        }
    }

    /**
     * Winner dice player [ ].
     *
     * @return the dice player [ ]
     */
    DicePlayer[] winner () {
        boolean win = true;
        DicePlayer[] winners = new DicePlayer[this.players.length];
        for (int i = 0; i < this.players.length; i++) {
            for (int j = 0; j < this.players.length; j++) {
                win = win && this.players[i].nbDiceRolls <= this.players[j].nbDiceRolls;
            }
            if (win) winners[i] = this.players[i];
            win = true;
        }
        return winners;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Dice dice = new Dice(6);
        NDicePlayerGame jeu = new NDicePlayerGame(Keyboard.readInt("Combien de joueur ? "));
        for (int i = 0; i < jeu.players.length; i++) {
            while (jeu.players[i].totalValue < 20) jeu.players[i].play(dice);
        }
        DicePlayer[] winners = jeu.winner();
        System.out.println("Les vainqueurs :");
        for (int i = 0; i < winners.length; i++) {
            if (winners[i] != null) {
                System.out.println("\t" + winners[i]);
            }
        }
    }
}

