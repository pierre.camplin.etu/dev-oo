package tp.tp02;

import java.util.Random;

/**
 * The type Dice.
 */
public class Dice {
    /**
     * The Number sides.
     */
    int numberSides,
    /**
     * The Value.
     */
    value;
    /**
     * The Rand.
     */
    Random rand;

    /**
     * Instantiates a new Dice.
     *
     * @param numberSides the number sides
     */
    Dice (int numberSides) {
        this.numberSides = (numberSides > 0) ? numberSides:1;
        this.value = 0;
        this.rand = new Random();
    }

    /**
     * Roll.
     */
    void roll () {
        this.value = this.rand.nextInt(this.numberSides) + 1;
    }
}