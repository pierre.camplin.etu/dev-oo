package tp.tpQU_4;

import tpQU.tp04.KeyboardException;
import tpQU.tp04.LocalKeyboard;

public class ExceptionPredefined {
    public static int[] tab = {17, 12, 15, 38, 29, 157, 89, -22, 0, 5};
    public static int division(int index, int divisor) {
        return tab[index]/divisor;
    }
    public static void statement() {
        int x, y;
        boolean f = false;
        do {
            try {
                x = LocalKeyboard.readInt("Write the index of the integer to divide: ");
                y = LocalKeyboard.readInt("Write the divisor: ");
                System.out.println("The result is: " + division(x,y));
            }
            catch (ArrayIndexOutOfBoundsException e) {System.err.println("Tab error");}
            catch (NullPointerException e) {System.err.println("Tab error");}
            catch (KeyboardException e) {System.err.println("Tab error");}
        } while (f);

    }
}
