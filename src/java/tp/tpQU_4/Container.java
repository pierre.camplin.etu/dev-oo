package tp.tpQU_4;

public enum Container {
    BLISTER(1), BOX(10), CRATE(50);

    private int capacity;

    Container(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity () {
        return this.capacity;
    }
}
