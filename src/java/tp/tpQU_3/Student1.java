package tp.tpQU_3;

/**
 * The type Student 1.
 */
public class Student1 extends Person{
    /**
     * The constant DEFAULT_DURATION.
     */
    public static final int DEFAULT_DURATION = 20;
    private int duration;

    /**
     * Instantiates a new Student 1.
     *
     * @param name     the name
     * @param forename the forename
     */
    public Student1(String name, String forename) {
        super(name, forename);
        this.duration = DEFAULT_DURATION;
    }

    /**
     * Instantiates a new Student 1.
     *
     * @param name     the name
     * @param forename the forename
     * @param duration the duration
     */
    public Student1(String name, String forename, int duration) {
        super(name, forename);
        this.duration = duration;
    }

    @Override
    public String toString() {
        return super.toString() + " (" + this.duration + ")";
    }
}
