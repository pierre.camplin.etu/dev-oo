package tp.tpQU_3;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

/**
 * The type Defense.
 */
public class Defense {
    /**
     * The When day.
     */
    private LocalDate whenDay;
    /**
     * The Where.
     */
    private Room where;
    /**
     * The Who.
     */
    private Student who;
    /**
     * The When hour.
     */
    private LocalTime whenHour;
    /**
     * The Title.
     */
    private String title;

    /**
     * Instantiates a new Defense.
     *
     * @param whenDay  the when day
     * @param where    the where
     * @param who      the who
     * @param whenHour the when hour
     * @param title    the title
     */
    public Defense(LocalDate whenDay, Room where, Student who, LocalTime whenHour, String title) {
        this.whenDay = whenDay;
        this.where = where;
        this.who = who;
        this.whenHour = whenHour;
        this.title = title;
    }

    /**
     * Instantiates a new Defense.
     *
     * @param where the where
     * @param who   the who
     * @param title the title
     */
    public Defense(Room where, Student who, String title) {
        this.where = where;
        this.who = who;
        this.title = title;
    }

    /**
     * Gets day.
     *
     * @return the day
     */
    public LocalDate getDay() {
        return whenDay;
    }

    /**
     * Gets place.
     *
     * @return the place
     */
    public Room getPlace() {
        return where;
    }

    /**
     * Gets student.
     *
     * @return the student
     */
    public Student getStudent() {
        return who;
    }

    /**
     * Gets hour.
     *
     * @return the hour
     */
    public LocalTime getHour() {
        return whenHour;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets day.
     *
     * @param whenDay the when day
     */
    public void setDay(LocalDate whenDay) {
        this.whenDay = whenDay;
    }

    /**
     * Sets hour.
     *
     * @param whenHour the when hour
     */
    public void setHour(LocalTime whenHour) {
        this.whenHour = whenHour;
    }

    /**
     * Gets duration.
     *
     * @return the duration
     */
    public int getDuration() {
        return who.getDuration();
    }

    @Override
    public String toString() {
        return this.whenDay + ":" + this.whenHour + " in " + this.where + " --> " + this.who + " :: " + this.title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Defense defense)) return false;

        if (!Objects.equals(whenDay, defense.whenDay)) return false;
        if (where != defense.where) return false;
        if (!Objects.equals(who, defense.who)) return false;
        if (!Objects.equals(whenHour, defense.whenHour)) return false;
        return Objects.equals(title, defense.title);
    }
}
