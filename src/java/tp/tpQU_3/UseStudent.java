package tp.tpQU_3;

/**
 * The type Use student.
 */
public class UseStudent {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Person alice = new Person("Alice", "a");
        StudentBUT1 bruno = new StudentBUT1("Bruno", "b");
        StudentBUT2 clement = new StudentBUT2("Clement", "c");
        System.out.println(bruno + "\n" + clement);
    }
}
