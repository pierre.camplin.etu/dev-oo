package tp.tpQU_3;

/**
 * The type Student.
 */
public abstract class Student extends Person{
    /**
     * The Duration.
     */
    private int duration;

    /**
     * Instantiates a new Student.
     *
     * @param name     the name
     * @param forename the forename
     * @param duration the duration
     */
    public Student(String name, String forename, int duration) {
        super(name, forename);
        this.duration = duration;
    }

    @Override
    public String toString() {
        return super.toString() + " (" + this.duration + ")";
    }

    /**
     * Gets duration.
     *
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }
}
