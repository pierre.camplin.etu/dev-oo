package tp.tpQU_3;

/**
 * The type Student but 2.
 */
public class StudentBUT2 extends Student{
    /**
     * The constant DEFAULT_DURATION.
     */
    public static final int DEFAULT_DURATION = 30;

    /**
     * Instantiates a new Student but 2.
     *
     * @param name     the name
     * @param forename the forename
     */
    public StudentBUT2(String name, String forename) {
        super(name, forename, DEFAULT_DURATION);
    }

    /**
     * Instantiates a new Student but 2.
     *
     * @param name     the name
     * @param forename the forename
     * @param duration the duration
     */
    public StudentBUT2(String name, String forename, int duration) {
        super(name, forename, duration);
    }
}
