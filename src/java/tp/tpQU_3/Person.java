package tp.tpQU_3;

/**
 * The type Person.
 */
public class Person {
    /**
     * The Name.
     */
    String name;
    /**
     * The Forename.
     */
    String forename;

    /**
     * Instantiates a new Person.
     *
     * @param name     the name
     * @param forename the forename
     */
    public Person(String name, String forename) {
        this.name = name;
        this.forename = forename;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets forename.
     *
     * @return the forename
     */
    public String getForename() {
        return forename;
    }

    @Override
    public String toString() {
        return this.forename + " " + this.name;
    }
}
