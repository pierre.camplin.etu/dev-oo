package tp.tpQU_3;


/**
 * The type Student but 1.
 */
public class StudentBUT1 extends Student{
    /**
     * The constant DEFAULT_DURATION.
     */
    public static final int DEFAULT_DURATION = 20;

    /**
     * Instantiates a new Student but 1.
     *
     * @param name     the name
     * @param forename the forename
     */
    public StudentBUT1(String name, String forename) {
        super(name, forename, DEFAULT_DURATION);
    }

    /**
     * Instantiates a new Student but 1.
     *
     * @param name     the name
     * @param forename the forename
     * @param duration the duration
     */
    public StudentBUT1(String name, String forename, int duration) {
        super(name, forename, duration);
    }
}
