package tp.tpQU_3;

/**
 * The type Student 2.
 */
public class Student2 extends Student1{
    /**
     * The constant DEFAULT_DURATION.
     */
    public static final int DEFAULT_DURATION = 30;
    private int duration;

    /**
     * Instantiates a new Student 2.
     *
     * @param name     the name
     * @param forename the forename
     * @param duration the duration
     */
    public Student2(String name, String forename, int duration) {
        super(name, forename);
        this.duration = duration;
    }

    /**
     * Instantiates a new Student 2.
     *
     * @param name     the name
     * @param forename the forename
     */
    public Student2(String name, String forename) {
        super(name, forename);
        this.duration = DEFAULT_DURATION;
    }
}
