package tp.tpQU_3;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 * The type Planning.
 */
public class Planning {
    /**
     * The When.
     */
    LocalDate when;
    /**
     * The Planning.
     */
    ArrayList<Defense> planning;
    /**
     * The Default start.
     */
    static LocalTime defaultStart = LocalTime.of(8, 0);
    /**
     * The Break duration.
     */
    static int breakDuration = 5;

    /**
     * Instantiates a new Planning.
     *
     * @param when the when
     */
    public Planning(LocalDate when) {
        this.when = when;
        this.planning = new ArrayList<>();
    }

    /**
     * Gets day.
     *
     * @return the day
     */
    public LocalDate getDay() {
        return when;
    }

    /**
     * Sets day.
     *
     * @param when the when
     */
    public void setDay(LocalDate when) {
        this.when = when;
    }

    /**
     * Add defense boolean.
     *
     * @param defense the defense
     * @return the boolean
     */
    public boolean addDefense (Defense defense) {
        return this.planning.add(defense);
    }

    /**
     * Add defense boolean.
     *
     * @param defenses the defenses
     * @return the boolean
     */
    public boolean addDefense (ArrayList<Defense> defenses) {
        return this.planning.addAll(defenses);
    }

    /**
     * Gets defense.
     *
     * @param idx the idx
     * @return the defense
     */
    public Defense getDefense (int idx) {
        if (idx < 0 || idx >= this.planning.size()) return null;
        return this.planning.get(idx);
    }

    /**
     * Gets nb defense.
     *
     * @return the nb defense
     */
    public int getNbDefense () {
        int i = 0;
        for (Defense defense:this.planning) {
            if (defense != null) i++;
        }
        return i;
    }

    /**
     * Scheduling.
     *
     * @param start the start
     */
    public void scheduling (LocalTime start) {
        for (Defense defense:
             this.planning) {
            defense.setHour(start);
            defense.setDay(this.when);
            start = start.plusMinutes(defense.getDuration() + breakDuration);
        }
    }

    /**
     * Scheduling.
     */
    public void scheduling () {
        this.scheduling(defaultStart);
    }

    /**
     * Swap defense.
     *
     * @param d1 the d 1
     * @param d2 the d 2
     */
    public void swapDefense (Defense d1, Defense d2) {
        int idx1 = this.getIdx(d1), idx2 = this.getIdx(d2);
        Defense tmp;
        LocalTime tmpTime;
        if (idx1 >= 0 && idx2 >= 0) {
            tmp = d1;
            this.planning.set(idx1, d2);
            this.planning.set(idx2, tmp);
            tmpTime = this.planning.get(idx1).getHour();
            this.planning.get(idx1).setHour(this.planning.get(idx2).getHour());
            this.planning.get(idx2).setHour(tmpTime);
        }
    }

    /**
     * Delay defense.
     *
     * @param d1           the d 1
     * @param minutesToAdd the minutes to add
     */
    public void delayDefense (Defense d1, int minutesToAdd) {

    }

    /**
     * Gets idx.
     *
     * @param d1 the d 1
     * @return the idx
     */
    public int getIdx (Defense d1) {
        int result = -1;
        for (int i = 0; i < this.planning.size(); i++) {
            if (this.planning.get(i).equals(d1)) {
                result = i;
            }
        }
        return result;
    }
}
