package tp.tpQU_3;

/**
 * The enum Room.
 */
public enum Room {
    /**
     * R 0 a 18 room.
     */
    r0A18,
    /**
     * R 0 a 20 room.
     */
    r0A20,
    /**
     * R 0 a 51 room.
     */
    r0A51,
    /**
     * R 0 a 49 room.
     */
    r0A49;

    @Override
    public String toString() {
        return "Room " + super.toString();
    }
}
